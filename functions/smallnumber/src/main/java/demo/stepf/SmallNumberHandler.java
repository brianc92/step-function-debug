package demo.stepf;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.function.aws.MicronautRequestHandler;

import java.sql.Timestamp;
import java.util.UUID;

@Introspected
public class SmallNumberHandler extends MicronautRequestHandler<Number, SmallNumber> {

    @Override
    public SmallNumber execute(Number input) {

        if(input.getNumber() < 0){
            throw new IllegalArgumentException("Number should not be less than 0. " +
                    "Provided number: " + input.getNumber());
        }

        var smallNumber = new SmallNumber();
        smallNumber.setNumber(input);
        smallNumber.setMessage("(test5) Handled small number at: " +
                new Timestamp(System.currentTimeMillis()).toString());
        return smallNumber;
    }
}
