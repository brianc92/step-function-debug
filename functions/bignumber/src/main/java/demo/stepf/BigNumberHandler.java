package demo.stepf;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.function.aws.MicronautRequestHandler;
import lombok.AllArgsConstructor;

import javax.inject.Inject;
import java.sql.Timestamp;

@Introspected
public class BigNumberHandler extends MicronautRequestHandler<Number, BigNumberResponse> {

    @Inject
    Service bigNumberService;

    @Override
    public BigNumberResponse execute(Number input) {
        return bigNumberService.saveBigNumber(input);
    }
}
