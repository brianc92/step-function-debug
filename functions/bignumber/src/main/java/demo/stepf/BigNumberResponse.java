package demo.stepf;

import io.micronaut.core.annotation.Introspected;

@Introspected
public class BigNumberResponse {

    public Number number;

    public String message;

    public Number getNumber() {
        return number;
    }

    public void setNumber(Number number) {
        this.number = number;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
