package demo.stepf;

import lombok.*;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;

@DynamoDbBean
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BigNumber {
    private String id;
    private int number;
    private String timestamp;
    private String message;
    private String test;

    @DynamoDbPartitionKey
    public String getId() {
        return id;
    }
}
