package demo.stepf;

import lombok.AllArgsConstructor;

import javax.inject.Singleton;
import java.sql.Timestamp;
import java.util.UUID;

@Singleton
@AllArgsConstructor
public class Service {

    DynamoClientProvider dynamoClientProvider;

    public BigNumberResponse saveBigNumber(Number num){

        var timestamp = new Timestamp(System.currentTimeMillis()).toString();
        var message = "Handled big number at: " + timestamp;

        dynamoClientProvider.getBigNumberTable()
                .putItem(new BigNumber(UUID.randomUUID().toString(), num.getNumber(), timestamp, message, UUID.randomUUID().toString().substring(0,5)));

        var bigNumberResponse = new BigNumberResponse();
        bigNumberResponse.setNumber(num);
        bigNumberResponse.setMessage(message);

        return bigNumberResponse;
    }
}
