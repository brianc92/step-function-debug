package demo.stepf;

import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
@Getter
public class DynamoClientProvider {

  private static final Logger log = LoggerFactory.getLogger(DynamoClientProvider.class);

  // look into: io.micronaut.aws.sdk.v2.service.dynamodb
  // DynamoDB client factory.
  private final DynamoDbClient dynamoClient;

  private final DynamoDbEnhancedClient enhancedClient;

  private final TableSchema<BigNumber> bigNumberTableSchema;

  private final String tableName;

  @Inject
  public DynamoClientProvider(DynamoDbClient dynamoClient) {
    this.dynamoClient = dynamoClient;
    this.enhancedClient =
        DynamoDbEnhancedClient.builder().dynamoDbClient(this.dynamoClient).build();
    this.bigNumberTableSchema = TableSchema.fromBean(BigNumber.class);
    this.tableName = System.getenv("BigNumberTableName");
  }

  public DynamoDbTable<BigNumber> getBigNumberTable() {
    return this.enhancedClient.table(tableName, this.bigNumberTableSchema);
  }
}
