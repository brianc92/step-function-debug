package demo.stepf;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.function.aws.MicronautRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

@Introspected
public class RandomNumberHandler extends MicronautRequestHandler<NumberRange, Number> {
    private final static Logger log = LoggerFactory.getLogger(RandomNumberHandler.class);

    @Override
    public Number execute(NumberRange range) {
        log.info("Test 3");

        Number num = new Number();
        num.setNumber(ThreadLocalRandom.current().nextInt(range.getMin(), range.getMax()));
        return num;
    }

}
