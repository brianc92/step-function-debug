package demo.stepf;

import io.micronaut.core.annotation.Introspected;

@Introspected
public class NumberRange {

    private int max;

    private int min;

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }
}
