# Step Function Local Dev Example

This README contains an example dev environment for Step Functions & Lambdas that allows for debugging

## Contents

1. Set Up
    1. Making a dev SAM template
    2. sam build
2. Debugging Lambdas
    1. Debug one at a time
    2. Debug all at once
3. Debugging Step Functions
    1. Running a step function locally
    2. Debugging a step function locally
4. Deploying Step Function Project
5. Denying Cloudformation permission to replace or delete the DB Table
6. Deployment Configuration
    1. Overview / How it works
    2. Configuring Pre/Post deployment hooks
    3. Configuring Alarms

## Set Up

### Making a dev SAM template

The file `demplate-dev.yaml` is used in this example to build/compile the the lambdas

The file `template-dev-placeholders.yaml` is provided to describe a basic configuration for local development. It follows the below structure:

```yaml
Transform: AWS::Serverless-2016-10-31

Resources:
  <FUNCTION-NAME>:
    Type: AWS::Serverless::Function
    Properties:
      Timeout: 300
      CodeUri: <FUNCTION-PROJECT-ROOT-FOLDER>
      Handler: <FUNCTION-HANDLER-FULLY-QUAL-NAME>
      Runtime: java8
      MemorySize: 512
  <FUNCTION-2-NAME>:
      .... etc etc
```

`<FUNCTION-NAME>` eg: RandomNumberFunction

`<FUNCTION-PROJECT-ROOT-FOLDER>` can be a relative or absolute path to source code root folder

`<FUNCTION-HANDLER-FULLY-QUAL-NAME>` name of the java class that handles the lambda event, eg: demo.stepf.RandomNumberHandler

The file `template-dev.yaml` which is used in this example follows this format.

**Note:** Its perhaps obvious but the SAM template requirements for deploying into prod are different from the local dev environment, this is just covering the local dev environment

### sam build

Running this command at the root of this repo

```bash
sam build --template ./template-dev.yaml
```

Will create a folder called `.aws-sam` in the current directory to store bineries and config files. The `template-dev.yaml` file is used to direct the sam cli towards the Lambda source code. `template-dev.yaml` is then reformatted by the `sam cli` program and saved in `.aws-sam/build`

When running subsequent development processes, the generated template in `.aws-sam/build/template.yaml` should be used.

## Debugging Lambdas

### Debug one at a time

The best approach for debugging one at a time is to use the IntelliJ plugin: Intellij AWS Toolkit <https://aws.amazon.com/intellij/>

However if it needs to be done manually, here are the steps:

1. Make a SAM template which at least specifies the lambda we are developing and put in the root directory of the project
2. Make a json file representing the 'event' that the lambda is consuming, for example see in `events/get-random-number-event.json`
3. Run `sam build --template ./template-dev.yaml` in the project root directory
4. Run `sam local invoke RandomNumberFunction --template ./.aws-sam/build/template.yaml --event events/get-random-number-event.json --debug-port 5055 --debug`. ( `-debug` flag is added to set log output)
5. The program will pause while you attach the debugger

Alternativly... adapt the below main method to call your query handler class in the IDE:

```java
  public static void main(String[] args) {
    Micronaut.run(TransactionQueryHandler.class, args)
        .getBean(TransactionQueryHandler.class)
        .execute(new QueryParams(2, "2021#01#01", "COSTA"));
  }
```

Or, use the unit testing framework:

```java
  @MicronautTest
  class MyTest {

    @Inject MyRequestHandler requestHandler;

    @Test
    void run(){
      requestHander.execute("example");
    }

  }
```

### Debug all at once

1. Navigate to a directory that is not the root directory of any of the lambdas - this is more for tidiness than anything else. The root of this directory will be used here
2. Make a SAM template that specifies all the lambdas you want to test. `template-dev.yaml` is the example here
3. Run `sam build --template ./template-deploy.yaml`
4. Run `sam local start-lambda -t ./.aws-sam/build/template.yaml --debug-port 5055 --debug`. This starts up all the lambdas
5. Invoke the lambda externally running the following command in this directory

    ```bash
    aws lambda invoke --function-name "RandomNumberFunction" --endpoint-url "http://127.0.0.1:3001" --payload "$(<events/get-random-number-event.json | base64)" response.json

    aws lambda invoke --function-name "BigNumberFunction" --endpoint-url "http://127.0.0.1:3001" --payload "$(<response.json | base64)" response.json
    ```

6. The SAM cli program will pause the execution of the specified function until the debugger is attached
7. The response is output in the response.json file

## Debugging Step Functions

### Running a step function locally

1. Follow the instructions to download the StepFunctionsLocal.jar <https://docs.aws.amazon.com/step-functions/latest/dg/sfn-local.html>  
   This tutorial assumes an environment variable `$STEP_FUNCTIONS_LOCAL` is set pointing to the jar
2. Start up all the lambdas by following the instructions in *Debugging Lambdas -> Debug all at once*. On Step 4 you can disable debug mode by removing `--debug-port 5055`.
3. Run the StepfunctionsLocal jar, `java -jar $STEP_FUNCTIONS_LOCAL --lambda-endpoint http://localhost:3001`
4. Execute the following commands in this directory to see the step function in action:

    ```bash
    aws stepfunctions --endpoint http://localhost:8083 create-state-machine --definition "$(<step-function/numberprocess-dev.asl.json)" --name "NumberProcess" --role-arn "arn:aws:iam::012345678901:role/DummyRole"

    aws stepfunctions --endpoint http://localhost:8083 start-execution --state-machine "arn:aws:states:us-east-1:123456789012:stateMachine:NumberProcess" --name test
    ```

    You should be able to see log output from the terminals running the lambdas and StepFunctionLocal.jar confirming that it is working

### Debugging a step function locally

1. Follow *Debugging Step Functions -> Running a step function locally* steps 1-2
2. Start StepFunctionsLocal in debug: `java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=\*:5066 -jar $STEP_FUNCTIONS_LOCAL --lambda-endpoint http://localhost:3001`
3. Configure IntelliJ to debug this by:
    1. Creating a new project
        1. Select file -> new -> java project, create the project
        2. Set project name, module name and location to whatever, confirm selection
    2. Adding StepFunctionsLocal.jar as a dependency
        1. click on the new project
        2. select file -> project structure
        3. within project settings select modules
        4. select newly created module
        5. select the 'dependencies' tab
        6. select '+' -> JARs or directories
        7. select the following .jse files:
                1.StepFunctionsLocal.jar - contains the main logic of step functions execution
                2.AWSStepFunctionsJavaClient-1.11.x.jar - contains the logging event objects, which may come come in useful
        8. in the project view, select External Libraries -> StepFunctionsLocal to explore the code
    3. Setting a breakpoint
        1. Set breakpoint in the 'execute' method of com.amazonaws.stepfunctions.local.runtime.executors.StateMachineExecutor::execute() line 81 and 82
    4. Setting up a debug configuration
        1. Open Run/Debug Configurations dialogue box by selecting Run -> Edit Configurations
        2. select '+' -> Remote JVM Debug
        3. In this example, fort is set to 5066
        4. Make sure the 'Use module classpath' selects the module name created earlier, press ok
4. Connect to the debug port
5. Run the following command to catch the breakpoint:

    ```bash
    aws stepfunctions --endpoint http://localhost:8083 create-state-machine --definition "$(<step-function/numberprocess-dev.asl.json)" --name "NumberProcess" --role-arn "arn:aws:iam::012345678901:role/DummyRole"

    aws stepfunctions --endpoint http://localhost:8083 start-execution --state-machine "arn:aws:states:us-east-1:123456789012:stateMachine:NumberProcess" --name test
    ```

6. When writing step functions you may need to access some environmental 'context' objects using JSONPath syntax... ideally you should be able to know exactly what the contextual objects have in them  

    > If the field value begins with only one "$", the value MUST be a Path. In this case, the Path is applied to the **Payload Templates input** and is the new field value.  
    > If the field value begins with "$$", the first dollar sign is stripped and the remainder MUST be a Path. In this case, the Path is applied to the **Context Object** and is the new field value.  
    > *from the docs: <https://states-language.net/spec.html>*  

    With the provided breakpoint in step 3.1, you can inspect them:  
    1. the `Payload Template's input` which is called the *input*  
    2. the `Context Object`, which is called the *executionModel*  

## Deploying Step Function Project

Running `sam build -t template-deploy.yaml` and then `sam deploy --config-file samconfig-deploy.toml` in this directory will deploy to AWS.

Some notes:

* `sam deploy` uses the `template.yaml` within `.aws-sam/build`
* `template-deploy.yaml` contains the SAM specification of the step function
* the `numberprocess.asl.json` (used to define the step function) contains variables that point to the lambdas under use, these variables re defined in `template-deploy.yaml`
* `samconfig-deploy.toml` was generated by following `sam deploy --guided` and determins where the config & bineries are saved.

## Denying Cloudformation permission to replace or delete the DB Table

The API doc is available here <https://awscli.amazonaws.com/v2/documentation/api/latest/reference/cloudformation/set-stack-policy.html> There are beter ways to do this however the only command I could get to work was:

```bash
aws cloudformation set-stack-policy --cli-input-json "{\"StackName\":\"NumberProcess\",\"StackPolicyBody\":\"{\\\"Statement\\\":[{\\\"Effect\\\":\\\"Allow\\\",\\\"Action\\\":\\\"Update:*\\\",\\\"Principal\\\":\\\"*\\\",\\\"Resource\\\":\\\"*\\\"},{\\\"Effect\\\":\\\"Deny\\\",\\\"Action\\\":[\\\"Update:Replace\\\",\\\"Update:Delete\\\"],\\\"Principal\\\":\\\"*\\\",\\\"Resource\\\":\\\"*\\\",\\\"Condition\\\":{\\\"StringEquals\\\":{\\\"ResourceType\\\":[\\\"AWS::RDS::DBInstance\\\",\\\"AWS::DynamoDB::Table\\\"]}}}]}\"}"
```

The json string is a strigified version of the below json:

```json
{
    "StackName": "NumberProcess",
    "StackPolicyBody": "{\"Statement\":[{\"Effect\":\"Allow\",\"Action\":\"Update:*\",\"Principal\":\"*\",\"Resource\":\"*\"},{\"Effect\":\"Deny\",\"Action\":[\"Update:Replace\",\"Update:Delete\"],\"Principal\":\"*\",\"Resource\":\"*\",\"Condition\":{\"StringEquals\":{\"ResourceType\":[\"AWS::RDS::DBInstance\",\"AWS::DynamoDB::Table\"]}}}]}"
}
```

the *StackPolicyBody* attribute is itself a strigified policy configuration:

```json
{
  "Statement" : [
    {
      "Effect" : "Allow",
      "Action" : "Update:*",
      "Principal": "*",
      "Resource" : "*"
    },
    {
      "Effect" : "Deny",
      "Action" : ["Update:Replace", "Update:Delete"],
      "Principal": "*",
      "Resource" : "*",
      "Condition" : {
          "StringEquals" : {
            "ResourceType" : ["AWS::RDS::DBInstance", "AWS::DynamoDB::Table"]
          }
        }
    }
  ]
}
```

This policy allows all actions except replace and delete on database resource types, more info here: <https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/protect-stack-resources.html#stack-policy-reference>

## Deployment Configuration

### Overview / How it works

When deploying with CodeDeploy, there are three stages to deployment, which corrospond to three possible resources you can configure to automatically rollback the deployment:

| Stage          | Resource     |
| -----------    | -----------  |
| Pre Deploy     | Lambda Hook  |
| Traffic Shift  | Alarm        |
| Post Deploy    | Lambda Hook  |

Lambda Hook - a lambda function you can do what you want with and then finish by sending an event which contains a success or fail flag. The deploy will wait indefinatly until the event arrives  
Alarm - A listener which is configured to listen for specific metrics associated with whatever you want, it just makes sense to point it towards the deployed function

### Configuring Pre/Post deployment hooks

These just look like regular lambdas but with some extra parmissions:

```yaml
  SmallNumberPostDeploy:
    Type: AWS::Serverless::Function
    Properties:
      <normal properties here>
      Environment:
        Variables:
          SmallNumberFunctionArn: !Ref SmallNumberFunction.Version 
          IntegrationTestInput: 1
      Policies: 
        - LambdaInvokePolicy: 
            FunctionName: !Ref SmallNumberFunction <-- permission to invoke 
        - Version: "2012-10-17"
          Statement:
            - Effect: "Allow"
              Action:
                - "codedeploy:PutLifecycleEventHookExecutionStatus" <-- permission to send the event to CodeDeploy to confirm succes/fail
              Resource: "*"
```

### Configuring Alarms

Easiest way to understand an alarm is to make one through the console so you can see the variety of metrics / configurations for it.  

``` yaml
  SmallNumberFunctionMostRecentErrorAlarm:
    Type: "AWS::CloudWatch::Alarm"
    Properties:
      Dimensions:  <-- these point the alarm towards whatever
        - Name: FunctionName
          Value: !Ref SmallNumberFunction
        - Name: Resource
          Value: !Sub "${SmallNumberFunction}:${SmallNumberFunction.Version.Version}" <-- this specifies the most recent version of the lambda
      AlarmDescription: Lambda Function Error > 0   <-- everything below here is easier to understand by looking at its equivalent property in the UI
      ComparisonOperator: GreaterThanThreshold
      EvaluationPeriods: 1
      MetricName: Errors
      Namespace: AWS/Lambda
      Period: 60
      Statistic: Sum
      Threshold: 0
```
