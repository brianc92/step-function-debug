package demo.hooks;
import com.amazonaws.services.codedeploy.model.LifecycleEventStatus;
import com.amazonaws.services.codedeploy.model.PutLifecycleEventHookExecutionStatusRequest;
import io.micronaut.core.annotation.Introspected;
import com.amazonaws.services.codedeploy.AmazonCodeDeployClientBuilder;
import io.micronaut.function.aws.MicronautRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@Introspected
public class RandomNumberPreDeployHandler extends MicronautRequestHandler<Map<String,String>, Void> {

    private static final Logger log = LoggerFactory.getLogger(RandomNumberPreDeployHandler.class);

    @Override
    public Void execute(Map<String,String> input) {
        log.info(input.toString());

        AmazonCodeDeployClientBuilder.defaultClient()
                .putLifecycleEventHookExecutionStatus(
                        new PutLifecycleEventHookExecutionStatusRequest()
                            .withStatus(LifecycleEventStatus.Succeeded)
                            .withLifecycleEventHookExecutionId(input.get("LifecycleEventHookExecutionId"))
                            .withDeploymentId(input.get("DeploymentId")));

        return null;
    }
}
