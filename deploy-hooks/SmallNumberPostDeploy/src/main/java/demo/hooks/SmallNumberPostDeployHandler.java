package demo.hooks;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.codedeploy.AmazonCodeDeployClientBuilder;
import com.amazonaws.services.codedeploy.model.LifecycleEventStatus;
import com.amazonaws.services.codedeploy.model.PutLifecycleEventHookExecutionStatusRequest;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.model.ServiceException;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.function.aws.MicronautRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.UUID;

@Introspected
public class SmallNumberPostDeployHandler extends MicronautRequestHandler<Map<String,String>, Void> {

    private static final Logger log = LoggerFactory.getLogger(SmallNumberPostDeployHandler.class);

    @Override
    public Void execute(Map<String,String> input) {
        log.info(input.toString());

        var testInput = Integer.parseInt(System.getenv("IntegrationTestInput"));
        var functionArn = System.getenv("SmallNumberFunctionArn");

        log.info("Targeting: {} with value: {}", functionArn, testInput);

        var request = new InvokeRequest()
                .withFunctionName(functionArn)
                .withPayload("{" +
                        "\"number\" :" + testInput +
                        "}");

        InvokeResult invokeResult = null;
        var status = LifecycleEventStatus.Succeeded;

        try {
            AWSLambda awsLambda = AWSLambdaClientBuilder.standard()
                    .withRegion(Regions.EU_WEST_1).build();

            invokeResult = awsLambda.invoke(request);

            String ans = new String(invokeResult.getPayload().array(), StandardCharsets.UTF_8);

            log.info("Function invocation response: {}", ans);

        } catch (ServiceException e) {
            log.error("Function invocation failed:", e);
            status = LifecycleEventStatus.Failed;
        }

        log.info("Function invocation status code: {}", invokeResult.getStatusCode());

        AmazonCodeDeployClientBuilder.defaultClient()
                .putLifecycleEventHookExecutionStatus(
                        new PutLifecycleEventHookExecutionStatusRequest()
                                .withStatus(status)
                                .withLifecycleEventHookExecutionId(input.get("LifecycleEventHookExecutionId"))
                                .withDeploymentId(input.get("DeploymentId")));

        return null;
    }
}
