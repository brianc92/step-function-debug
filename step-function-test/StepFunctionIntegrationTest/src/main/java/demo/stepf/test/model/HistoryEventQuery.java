package demo.stepf.test.model;

import com.amazonaws.services.stepfunctions.model.HistoryEvent;
import com.amazonaws.services.stepfunctions.model.HistoryEventType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class HistoryEventQuery {

  /**
   * Limitation here is that step function example does not contain the variety of possible uses and
   * so the events were testing with are not good enough
   *
   * <p>test step function data needs as least: parallelism, multiple calls to the same step
   */
  private List<HistoryEvent> events;

  public List<HistoryEvent> getStepEvents(String stepName) {
    // loop through until TaskStateExited, and then trace back to origin TaskStateEntered,
    // collecting events in chain
    // this code does not account for multiple executions of the same step, returning the first
    // execution only

    var stepEvents = new ArrayList<HistoryEvent>();

    for (var i = 0; i < events.size(); i++) {
      var event = events.get(i);
      if (event.getStateExitedEventDetails() != null
          && event.getStateExitedEventDetails().getName().equals(stepName)) {
        stepEvents.add(event);

        int innerIndex = i;
        long targetEventId = event.getPreviousEventId();
        while (true) {
          event = events.get(--innerIndex);
          var isStateEntered = HistoryEventType.TaskStateEntered.toString().equals(event.getType());

          if (event.getId() == targetEventId) {
            targetEventId = event.getPreviousEventId();
            stepEvents.add(event);
            if (isStateEntered) {
              return stepEvents;
            }
          }

          if (event.getId() == 0L) {
            throw new IllegalStateException(
                "Failed to parse event history - should not reach the lowest element");
          }
        }
      }
    }

    throw new IllegalStateException(
        "Failed to parse event history - should not reach the end of event list");
  }

  public List<String> getStepNamesFollowingStep(String stepName) {
    // assumes step has only been executed once
    var stepNames = new ArrayList<String>();

    for (var event : events) {
      var targetIndex = -1L;

      if (event.getStateExitedEventDetails() != null
          && event.getStateExitedEventDetails().getName().equals(stepName)) {
        targetIndex = event.getId();
      }

      if (targetIndex == event.getPreviousEventId()
          && event.getStateEnteredEventDetails() != null) {
        stepNames.add(event.getStateEnteredEventDetails().getName());
      }
    }

    return stepNames;
  }
}
